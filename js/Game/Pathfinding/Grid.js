class Grid {
	constructor(map, baseNode) {
		this.map= map;
		this.baseNode= baseNode;
		this.grid= null;
		this.gridSizeX= 0;
		this.gridSizeY= 0;
	}

	createGrid() {
		var grid= [];
		this.gridSizeX= this.map.largeur;
		this.gridSizeY= this.map.hauteur;

		for (var y= 0; y < this.map.hauteur; ++y){
			var rangee= [];

			for (var x= 0; x < this.map.largeur; ++x){
				var node= new Node(this.isWalkable(this.map.tuiles[y][x]), this.map.tuiles[y][x]); //temp
				rangee.push(node);
			}

			grid.push(rangee);
		}

		this.grid= grid;
	}

	getNeighbours(node) {
		var neighbours= [];

		for (var x= -1; x <= 1 ; x++) {
			for (var y= -1; y <= 1; y++) {
				if (!(x == 0 && y == 0)) {
					if (x == 0 || y == 0) {
						var checkX= node.tuile.posX + x;
						var checkY= node.tuile.posY + y;
					}

					if (checkX >= 0 && checkX < this.gridSizeX && checkY >= 0 && checkY < this.gridSizeY) {
						neighbours.push(this.grid[checkY][checkX]); 
					}
				}
			}
		}	

		return neighbours;
	}

	isWalkable(tuile) {
		var isWalkable= false;
		var unitMaster= this.baseNode.tuile.unit.master;

		if (tuile.cotePassage != null) {
			if (tuile.unit == null) {
				isWalkable= true;
			}
			else if (tuile.unit.master == unitMaster) {
				isWalkable= true;
			}
		}

		return isWalkable;
	}
}