class Pathfinding {
	constructor(map) {
		this.map= map;
	}

	findPath(start, target) {
		var startNode= new Node(true, start);
		var grid= new Grid(this.map, startNode);
		grid.createGrid();
		var targetNode= grid.grid[target.posY][target.posX];

		var openSet= [];
		var closedSet= [];
		var neighbours= [];
		
		openSet.push(startNode);

		while (openSet.length > 0 && targetNode.tuile.cotePassage < start.unit.mov) {
			var currentNode= openSet[0];
				
			for (var i = 0; i < openSet.length; i++) {			
				if (openSet[i].fCost() < currentNode.fCost() 
					|| openSet[i].fCost() == currentNode.fCost() 
					&& openSet[i].hCost < currentNode.hCost) {
					currentNode= openSet[i];
				}
			}

			this.removeFromArray(openSet, currentNode);
			closedSet.push(currentNode);

			if (currentNode == targetNode) {
				return this.retracePath(startNode, targetNode);
			}

			neighbours= grid.getNeighbours(currentNode)
			neighbours.forEach(function(neighbour) {
				if (!neighbour.walkable || this.isInSet(neighbour, closedSet)) {
					//rien
				}
				else {
					var newMovCostToNeighbour= currentNode.gCost + this.getDistance(currentNode, neighbour) + currentNode.tuile.cotePassage * 10;

					if (newMovCostToNeighbour < neighbour.gCost || !this.isInSet(neighbour, openSet)) {
						neighbour.gCost= newMovCostToNeighbour;
						neighbour.hCost= this.getDistance(neighbour, targetNode);
						neighbour.parent= currentNode;

						if (!this.isInSet(neighbour, openSet) && neighbour.gCost <= start.unit.mov * 10) {
							openSet.push(neighbour);
						}
					}
				}
				
			}, this);
		}
	}

	getPath(start, target) {
		var nodePath= this.findPath(start, target);
		var path= [];

		nodePath.forEach(function(node) {
			path.push(node.tuile);
		}, this);

		
		return path;
	}

	findActionRange(caseDep) {
		var basicRange= this.getBasicRange(caseDep);
		var caseDep= caseDep;
		var actRange= [];
		var path= null;
		
		basicRange.forEach(function(tuile) {
			path= null;
			path= this.findPath(caseDep, tuile);
			

			if (path != null) {
				path.forEach(function(node) {
					if (!this.isInRange(node, actRange)) {
						if(node.gCost <= caseDep.unit.mov * 10) {
							actRange.push(node.tuile);
						}
					}
				}, this);
			}
		}, this);

		return actRange;
	}

	getBasicRange(caseDep) {
		var range= [];
		var tuile= null;

		for(var y = -caseDep.unit.mov; y <= caseDep.unit.mov; y++) {
			for(var x = -caseDep.unit.mov; x <= caseDep.unit.mov; x++) {
				var checkX= caseDep.unit.posX+x;
				var checkY= caseDep.unit.posY+y
				
				if (checkX >= 0 && checkX < this.map.largeur && checkY >= 0 && checkY < this.map.hauteur) {
					tuile= this.map.tuiles[checkY][checkX]; 

					if (this.calcDistanceTuiles(caseDep, tuile) <= caseDep.unit.mov && this.calcDistanceTuiles(caseDep, tuile) != 0) {
						range.push(tuile);
					}
				}
			}
		}

		return range;
	}

	calcDistanceTuiles(dep, arr) {
		var coteX= Math.sqrt(Math.pow(dep.posX-arr.posX, 2));
		var coteY= Math.sqrt(Math.pow(dep.posY-arr.posY, 2));
		var distance= coteX+coteY;

		return distance;
	}

	retracePath(startNode, endNode) {
		var path= [];
		var currentNode= endNode;

		while(currentNode != startNode) {
			path.push(currentNode);
			currentNode= currentNode.parent;
		}

		path.reverse();
		return path;
	}

	getDistance(startNode, targetNode) {
		var distance= 0;
		var distX= Math.abs(startNode.tuile.posX - targetNode.tuile.posX);
		var distY= Math.abs(startNode.tuile.posY - targetNode.tuile.posY);
		// var distance= coteX+coteY;

		if (distX > distY) {
			distance= 20 * distY + 10 * (distX - distY);
		}
		else {
			distance= 20 * distX + 10 * (distY - distX);
		}
		
		return distance;
	}

	isInSet(node, set) {
		var inSet= false;

		set.forEach(function(elem) {
			if (elem == node) {
				inSet= true;
			}
		}, this);

		return inSet;
	}

	isInRange(node, range) {
		var inRange= false;

		range.forEach(function(elem) {
			if (elem == node.tuile) {
				inRange= true;
			}
		}, this);

		return inRange;
	}

	removeFromArray(array, elem) {
		for (var i = 0; i < array.length; i++) {
			if (array[i] == elem) {
				array.splice(i, 1);
				i--;
			}
		}
	}
}