class Combat {
	constructor(offUnit, defUnit, map) {
		Combat.dmgModifier= 0.25;
		this.offUnit= offUnit;
		this.defUnit= defUnit;
		this.map= map;
		this.echanges= []; //tableau des échanges lors du combats. 1er élem => offUnit
		this.nbAtkOffUnit= 1;
		this.nbAtkDefUnit= 1;
		this.dmgOffUnit= 0;
		this.dmgDefUnit= 0;
		this.dmgModOffUnit= 0;
		this.dmgModDefUnit= 0;
	}

	fight() {
		var dead= null;

		this.calcNbEchanges();
		this.calcDmg();
		this.execute();

		if (this.offUnit.hp <= 0) {
			dead= this.offUnit;
		}

		if (this.defUnit.hp <= 0) {
			dead= this.defUnit;
		}

		return dead;
	}

	simulation() {
		var hpOffBefore= this.offUnit.hp;
		var hpDefBefore= this.defUnit.hp;
		var dmgDealt= {};

		this.calcNbEchanges();
		dmgDealt["nbAtkOff"]= this.nbAtkOffUnit;
		dmgDealt["nbAtkDef"]= this.nbAtkDefUnit;
		
		this.calcDmg();
		this.execute();

		dmgDealt["offHP"]= hpOffBefore - this.offUnit.hp;
		dmgDealt["defHP"]= hpDefBefore - this.defUnit.hp;

		this.offUnit.hp= hpOffBefore;
		this.defUnit.hp= hpDefBefore;

		return dmgDealt;
	}

	execute() {	
		while (this.nbAtkOffUnit > 0 || this.nbAtkDefUnit > 0) {
			if (this.nbAtkOffUnit > 0 && this.offUnit.hp > 0) {
				this.defUnit.hp-= this.dmgOffUnit;
			}

			if (this.nbAtkDefUnit > 0 && this.defUnit.hp > 0) {
				this.offUnit.hp-= this.dmgDefUnit;
			}
			
			this.nbAtkOffUnit--;
			this.nbAtkDefUnit--;
		}
	}

	calcDmg() {
		var dmgOff= 0;
		var dmgDef= 0;
		var bonusDefTuile= 0;	
		var tuileOff= this.map.tuiles[this.offUnit.posY][this.offUnit.posX];
		var tuileDef= this.map.tuiles[this.defUnit.posY][this.defUnit.posX];

		if (this.offUnit.weapon.type === Weapon.t_weaponType.PHYSICAL) {
			dmgOff= this.offUnit.atk - (this.defUnit.def + tuileDef.defModif);
		}
		else {
			dmgOff= this.offUnit.atk - (this.defUnit.res + tuileDef.defModif);
		}

		if (this.defUnit.weapon.type === Weapon.t_weaponType.PHYSICAL) {
			dmgDef= this.defUnit.atk - (this.offUnit.def + tuileOff.defModif);
		}
		else {
			dmgDef= this.defUnit.atk - (this.offUnit.res + tuileOff.defModif);
		}

		this.calcAvantage(); // calc dmgModOffUnit et dmgModDefUnit
		
		this.dmgOffUnit= Math.floor(dmgOff * this.dmgModOffUnit + dmgOff);
		this.dmgDefUnit= Math.floor(dmgDef * this.dmgModDefUnit + dmgDef);
	}

	calcNbEchanges () {
		if (this.offUnit.spd >= this.defUnit.spd + 5) {
			this.nbAtkOffUnit= 2;
		}
		else {
			this.nbAtkOffUnit= 1;
		}

		if (this.canCounter(this.offUnit, this.defUnit)) {
			if (this.defUnit.spd >= this.offUnit.spd + 5) {
				this.nbAtkDefUnit= 2;
			}
			else {
				this.nbAtkDefUnit= 1;
			}
		}
		else {
			this.nbAtkDefUnit= 0;
		}
	}

	canCounter(offUnit, defUnit) {
		var tuileOff= this.map.tuiles[offUnit.posY][offUnit.posX];
		var tuileDef= this.map.tuiles[defUnit.posY][defUnit.posX];
		return this.map.calcDistance(tuileOff, tuileDef) <= defUnit.weapon.range;
	}

	calcAvantage() {
		if (this.offUnit.weapon.weakness == this.defUnit.weapon.neutral) {
			this.dmgModOffUnit= -Combat.dmgModifier;
			this.dmgModDefUnit= Combat.dmgModifier;
		}
		else if (this.offUnit.weapon.strength == this.defUnit.weapon.neutral) {
			this.dmgModOffUnit= Combat.dmgModifier;
			this.dmgModDefUnit= -Combat.dmgModifier;
		}
	}
}