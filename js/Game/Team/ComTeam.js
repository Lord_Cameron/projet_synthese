class ComTeam {
	constructor() {
		this.teamSize= 17;
		this.groups= this.createTeam();
	}

	createTeam() {
		var team= [];
		var cptUnits= 0;
		var group= null;
		var unit= null
		
		group= new ComGroup();
		
		unit= new Knight("Boss", 22, 8, Unit.t_master.COM, Knight.t_image.COM);
		group.addToGroup(unit);
		
		unit= new Warrior("Warrior", 20, 8, Unit.t_master.COM, Warrior.t_image.COM);
		group.addToGroup(unit);
		
		unit= new Warrior("Warrior", 22, 10, Unit.t_master.COM, Warrior.t_image.COM);
		group.addToGroup(unit);
		
		unit= new Mage("Mage", 21, 9, Unit.t_master.COM, Mage.t_image.COM);
		group.addToGroup(unit);

		team.push(group);
		
		unit= new Warrior("Warrior", 14, 5, Unit.t_master.COM, Warrior.t_image.COM);
		group.addToGroup(unit);
		
		unit= new Mercenary("Mercenary", 14, 6, Unit.t_master.COM, Mercenary.t_image.COM);
		group.addToGroup(unit);
		
		unit= new Mage("Mage", 13, 8, Unit.t_master.COM, Mage.t_image.COM);
		group.addToGroup(unit);

		team.push(group);
		
		unit= new Knight("Knight", 23, 17, Unit.t_master.COM, Knight.t_image.COM);
		group.addToGroup(unit);
		
		unit= new Cleric("Cleric", 23, 18, Unit.t_master.COM, Cleric.t_image.COM);
		group.addToGroup(unit);
		
		unit= new Shaman("Shaman", 21, 19, Unit.t_master.COM, Shaman.t_image.COM);
		group.addToGroup(unit);

		team.push(group);
		
		unit= new Shaman("Shaman", 1, 7, Unit.t_master.COM, Shaman.t_image.COM);
		group.addToGroup(unit);
		
		unit= new Mercenary("Mercenary", 14, 12, Unit.t_master.COM, Mercenary.t_image.COM);
		group.addToGroup(unit);

		team.push(group);
		
		unit= new Knight("Knight", 9, 9, Unit.t_master.COM, Knight.t_image.COM);
		group.addToGroup(unit);
		
		unit= new Knight("Knight", 9, 11, Unit.t_master.COM, Knight.t_image.COM);
		group.addToGroup(unit);
		
		unit= new Cleric("Cleric", 10, 10, Unit.t_master.COM, Cleric.t_image.COM);
		group.addToGroup(unit);

		team.push(group);
		
		unit= new Warrior("Warrior", 19, 12, Unit.t_master.COM, Warrior.t_image.COM);
		group.addToGroup(unit);
		
		unit= new Knight("Knight", 17, 15, Unit.t_master.COM, Knight.t_image.COM);
		group.addToGroup(unit);

		team.push(group);
		
		return team;
	}
}