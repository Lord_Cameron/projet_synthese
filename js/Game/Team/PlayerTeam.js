class PlayerTeam {
	constructor() {
		this.teamSize= 10;
		this.units= this.createTeam();
	}

	createTeam(){
		var team= [];	
	
		team.push(new Knight("Oswin", 3, 16, 1, Knight.t_image.PLAYER)); //nom, x, y, master (1 == PLAYER)
		team.push(new Mercenary("Mia", 4, 16, 1, Mercenary.t_image.PLAYER));
		team.push(new Shaman("Canas",  3, 17, 1, Shaman.t_image.PLAYER));
		team.push(new Mage("Erk",  4, 17, 1, Mage.t_image.PLAYER));
		team.push(new Warrior("Hawkeye", 5, 16, 1, Warrior.t_image.PLAYER));

		team.push(new Cleric("Natasha", 6, 1, 1, Cleric.t_image.PLAYER));
		team.push(new Warrior("Haar", 8, 1, 1, Warrior.t_image.PLAYER));
		team.push(new Knight("Wallace", 8, 2, 1, Knight.t_image.PLAYER));
		team.push(new Mage("Pent", 7, 2, 1, Mage.t_image.PLAYER));
		team.push(new Mercenary("Lyn", 6, 2, 1, Mercenary.t_image.PLAYER));

		return team;
	}
}