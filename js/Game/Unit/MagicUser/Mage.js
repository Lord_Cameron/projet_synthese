class Mage extends MagicUser{
	constructor(nom, posX, posY, master, image) {
		Mage.t_image= {
			PORTRAIT : "images/sprites/mage.gif",
			PLAYER : "images/sprites/mage_player.png",
			NEUTRAL : "images/sprites/mage_neutral.png",
			COM : "images/sprites/mage_com.png",
		};
		super(nom, 18, 15, 27, 25, 18, 23, 3, posX, posY, new Anima(), master, image) 
	}

	getPortrait() {
		return Mage.t_image.PORTRAIT;
	}
}