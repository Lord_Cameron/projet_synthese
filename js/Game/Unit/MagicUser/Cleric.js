class Cleric extends MagicUser{
	constructor(nom, posX, posY, master, image) {
		Cleric.t_image= {
			PORTRAIT : "images/sprites/cleric.gif",
			PLAYER : "images/sprites/cleric_player.png",
			NEUTRAL : "images/sprites/cleric_neutral.png",
			COM : "images/sprites/cleric_com.png",
		};
		super(nom, 16, 15, 25, 27, 18, 23, 3, posX, posY, new Light(), master, image) 
	}

	getPortrait() {
		return Cleric.t_image.PORTRAIT;
	}
}