class Shaman extends MagicUser{
	constructor(nom, posX, posY, master, image) {
		Shaman.t_image= {
			PORTRAIT : "images/sprites/shaman.gif",
			PLAYER : "images/sprites/shaman_player.png",
			NEUTRAL : "images/sprites/shaman_neutral.png",
			COM : "images/sprites/shaman_com.png",
		};
		super(nom, 20, 15, 30, 23, 23, 18, 3, posX, posY, new Dark(), master, image) 
	}

	getPortrait() {
		return Shaman.t_image.PORTRAIT;
	}
}