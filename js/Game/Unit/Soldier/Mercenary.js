class Mercenary extends Soldier{
	constructor(nom, posX, posY, master, image) {
		Mercenary.t_image= {
			PORTRAIT : "images/sprites/mercenary.gif",
			PLAYER : "images/sprites/mercenary_player.png",
			NEUTRAL : "images/sprites/mercenary_neutral.png",
			COM : "images/sprites/mercenary_com.png",
		};
		super(nom, 20, 28, 15, 30, 20, 20, 4, posX, posY, new Sword(), master, image) //temp
	}

	getPortrait() {
		return Mercenary.t_image.PORTRAIT;
	}
}