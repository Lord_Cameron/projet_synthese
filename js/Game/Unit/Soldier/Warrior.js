class Warrior extends Soldier{
	constructor(nom, posX, posY, master, image) {
		Warrior.t_image= {
			PORTRAIT : "images/sprites/warrior.gif",
			PLAYER : "images/sprites/warrior_player.png",
			NEUTRAL : "images/sprites/warrior_neutral.png",
			COM : "images/sprites/warrior_com.png",
		};
		super(nom, 24, 30, 15, 25, 22, 15, 4, posX, posY, new Axe(), master, image) //temp
	}

	getPortrait() {
		return Warrior.t_image.PORTRAIT;
	}
}