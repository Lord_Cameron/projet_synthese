class Knight extends Soldier{
	constructor(nom, posX, posY, master, image) {
		Knight.t_image= {
			PORTRAIT : "images/sprites/knight.gif",
			PLAYER : "images/sprites/knight_player.png",
			NEUTRAL : "images/sprites/knight_neutral.png",
			COM : "images/sprites/knight_com.png",
		};
		super(nom, 22, 26, 15, 22, 25, 15, 4, posX, posY, new Lance(), master, image) //temp
	}

	getPortrait() {
		return Knight.t_image.PORTRAIT;
	}
}