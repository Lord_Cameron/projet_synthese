class Unit {
	constructor(nom, hp, str, mag, spd, def, res, mov, posX, posY, weapon, master, image, portrait){
		Unit.t_master= {
			PLAYER : 1,
			COM : 2,
		};
		this.nom= nom;
		this.hp= hp;
		this.totalHp= hp;
		this.str= str;
		this.mag= mag;
		this.spd= spd;
		this.def= def;
		this.res= res;
		this.mov= mov;
		this.posX= posX;
		this.posY= posY;
		this.weapon= weapon;
		this.master= master;
		this.image= image;
		this.portrait= portrait;
		this.aAgit= false;
		this.remainMov= mov;
		this.movRange= null;
		this.anim= new UnitAnim(image, posX, posY, this.master == Unit.t_master.COM);
		this.randomizeStats();
		this.atk= this.calcAtk();
	}

	randomizeStats() {
		var bonus= 0;
		if (this.master == Unit.t_master.PLAYER) {
			bonus= 0;
		}

		this.hp= this.hp + Math.floor(Math.random() * 10) + 5;
		this.totalHp= this.hp;
		this.str= (this.str - Math.floor(Math.random()*(5-0+1)+0))+bonus;
		this.mag= (this.mag - Math.floor(Math.random()*(5-0+1)+0))+bonus;
		this.spd= (this.spd - Math.floor(Math.random()*(5-0+1)+0))+bonus;
		this.def= (this.def - Math.floor(Math.random()*(5-0+1)+0))+bonus;
		this.res= (this.res - Math.floor(Math.random()*(5-0+1)+0))+bonus;
	}

	changeMaster(master) {
		this.master= master;
	}

	switchAction(){
		this.aAgit= !aAgit;
	}

	changeMov(travelled) {
		this.remainMov-= travelled;
		
		if (this.remainMov < 0) {
			this.remainMov= 0;
		}
	}

	restoreMov(restored) {
		this.remainMov+= restored;
		
		if (this.remainMov > this.mov) {
			this.remainMov= this.mov;
		}
	}

	isInMovRange(tuile) {
		var inRange= false;
		this.movRange.forEach(function(elem) {
			if (tuile == elem) {
				inRange= true;
			}
		}, this);

		return inRange;
	}

	calcAtk() {
		var atk= 0;
		
		if (this instanceof Soldier) {
			atk= this.str + this.weapon.force;
		}
		else {
			atk= this.mag + this.weapon.force;
		}

		return atk;
	}
}