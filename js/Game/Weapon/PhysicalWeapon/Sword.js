class Sword extends PhysicalWeapon{
	constructor() {
		super(6)
		this.weakness= Weapon.t_weapons.LANCE;
		this.neutral= Weapon.t_weapons.SWORD;
		this.strength= Weapon.t_weapons.AXE;
	}
}