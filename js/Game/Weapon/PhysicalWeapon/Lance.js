class Lance extends PhysicalWeapon{
	constructor() {
		super(7)
		this.weakness= Weapon.t_weapons.AXE;
		this.neutral= Weapon.t_weapons.LANCE;
		this.strength= Weapon.t_weapons.SWORD;
	}
}