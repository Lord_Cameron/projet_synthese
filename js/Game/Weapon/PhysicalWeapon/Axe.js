class Axe extends PhysicalWeapon{
	constructor() {
		super(8)
		this.weakness= Weapon.t_weapons.SWORD;
		this.neutral= Weapon.t_weapons.AXE;
		this.strength= Weapon.t_weapons.LANCE;
	}
}