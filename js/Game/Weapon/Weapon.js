class Weapon {
	constructor(force, range, type) {
		Weapon.t_weapons= {
			SWORD : 1,
			AXE : 2,
			LANCE : 3,
			ANIMA : 4,
			DARK : 5,
			LIGHT : 6,
		};
		Weapon.t_weaponType= {
			MAGICAL : 1,
			PHYSICAL : 2,
		};
		this.force= force;
		this.range= range;
		this.type= type;
	}
}