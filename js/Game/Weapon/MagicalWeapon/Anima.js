class Anima  extends MagicWeapon{
	constructor() {
		super(7)
		this.weakness= Weapon.t_weapons.DARK;
		this.neutral= Weapon.t_weapons.ANIMA;
		this.strength= Weapon.t_weapons.LIGHT;
	}
}