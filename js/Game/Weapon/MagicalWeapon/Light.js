class Light  extends MagicWeapon{
	constructor() {
		super(6)
		this.weakness= Weapon.t_weapons.ANIMA;
		this.neutral= Weapon.t_weapons.LIGHT;
		this.strength= Weapon.t_weapons.DARK;
	}
}