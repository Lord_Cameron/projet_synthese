class Dark extends MagicWeapon{
	constructor() {
		super(8)
		this.weakness= Weapon.t_weapons.LIGHT;
		this.neutral= Weapon.t_weapons.DARK;
		this.strength= Weapon.t_weapons.ANIMA;
	}
}