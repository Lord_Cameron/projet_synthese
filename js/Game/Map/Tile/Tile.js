class Tile {
	constructor(defModif, posX, posY, cotePassage) {
		Tile.t_colors= {
			RED : "rgba(200,0,0,",
			GREEN : "rgba(0,200,0,",
			BLUE : "rgba(0,0,200,",
		}
		this.size= 30;
		this.unit = null;
		this.defModif= defModif;
		this.posX= posX;
		this.posY= posY;
		this.color= null;
		this.cotePassage= cotePassage;
		this.opacity= 0.5;
	}

	setColorTo(color) {
		if (color === null) {
			this.color= null;
		}
		else {
			this.color= color + this.opacity + ")";
		}
	}

	getCotePassage(){ //ne marche que pour le joueur
		var coteTotale= this.cotePassage;

		if (this.unit != null) {
			if (this.unit.master == Unit.t_master.COM) {
				coteTotale+= 100;
			}
		}

		return coteTotale;
	}
}