class Map {
	constructor() {
		this.hauteur= 600/30;
		this.largeur= 900/30;
		this.tuiles= this.createMap();
	}

	createMap() {
		var tuiles= [];

		for (var y= 0; y < this.hauteur; ++y) {
			var rangee= [];

			for (var x= 0; x < this.largeur; ++x) {
				var tuile= new FieldTile(x, y);

				// //NoneTiles
				if (x >= 13 && x <= 15 && y >= 3 && y <= 5) {
					if (!(x == 14 && y == 5)) {
						tuile= new NoneTile(x, y);
					}
				}
				
				if (x >= 21 && x <= 23 && y >= 6 && y <= 8) {
					if (!(x == 22 && y == 8)) {
						tuile= new NoneTile(x, y);
					}
					else {
						tuile= new CastleTile(x, y);
					}
				}

				if (x >= 22 && x <= 24 && y >= 15 && y <= 17) {
					if (!(x == 23 && y == 17)) {
						tuile= new NoneTile(x, y);
					}
				}

				// //CastleTiles
				if (x == 1 && y == 7) {
					tuile= new CastleTile(x, y);
				}

				if (x == 14 && y == 12) {
					tuile= new CastleTile(x, y);
				}

				if (x == 15 && y == 12) {
					tuile= new CastleTile(x, y);
				}

				if (x == 18 && y == 6) {
					tuile= new CastleTile(x, y);
				}

				if (x == 20 && y == 8) {
					tuile= new CastleTile(x, y);
				}

				if (x == 22 && y == 10) {
					tuile= new CastleTile(x, y);
				}

				if (x == 25 && y == 11) {
					tuile= new CastleTile(x, y);
				}

				if (x == 24 && y == 5) {
					tuile= new CastleTile(x, y);
				}

				// // ForestTiles and RiverTiles
				switch(y) {
					case 0:
						if (x == 0 || x  == 5 || x  == 10 || x  == 5 || x >= 14 && x <= 19 || x >= 23 && x <= 25 || x >= 27 && x <= 28) {
							tuile= new ForestTile(x, y);
						}
						break;
					case 1:
						if (x == 2 || x  == 15 || x >= 21 && x <= 24 || x >= 27 && x <= 28) {
							tuile= new ForestTile(x, y);
						}
						break;
					case 2:
						if (x == 28) {
							tuile= new ForestTile(x, y);
						}
						if (x >= 17 && x <= 20 || x >= 25 && x <= 26) {
							tuile= new RiverTile(x, y);
						}
						break;
					case 3:
						if (x == 3 || x == 11) {
							tuile= new ForestTile(x, y);
						}
						if (x >= 16 && x <= 27) {
							tuile= new RiverTile(x, y);
						}
						break;
					case 4:
						if (x == 3 || x == 8) {
							tuile= new ForestTile(x, y);
						}
						if (x >= 16 && x <= 29) {
							tuile= new RiverTile(x, y);
						}
						break;
					case 5:
						if (x == 4 || x == 5) {
							tuile= new ForestTile(x, y);
						}
						if (x >= 16 && x <= 23 || x >= 26 && x <= 29) {
							tuile= new RiverTile(x, y);
						}
						break;
					case 6:
						if (x == 4 || x == 5 || x == 29) {
							tuile= new ForestTile(x, y);
						}
						if (x >= 25 && x <= 28) {
							tuile= new RiverTile(x, y);
						}
						break;
					case 7:
						if (x == 2 || x == 5 || x == 10) {
							tuile= new ForestTile(x, y);
						}
						if (x >= 24 && x <= 29) {
							tuile= new RiverTile(x, y);
						}
						break;
					case 8:
						if (x == 10 || x == 11 || x == 13 || x == 19) {
							tuile= new ForestTile(x, y);
						}
						if (x >= 24 && x <= 29) {
							tuile= new RiverTile(x, y);
						}
						break;
					case 9:
						if (x == 13) {
							tuile= new ForestTile(x, y);
						}
						if (x >= 25 && x <= 29) {
							tuile= new RiverTile(x, y);
						}
						break;
					case 10:
						if (x == 18 || x == 20) {
							tuile= new ForestTile(x, y);
						}
						if (x >= 25 && x <= 29) {
							tuile= new RiverTile(x, y);
						}
						break;
					case 11:
						if (x >= 26 && x <= 29) {
							tuile= new RiverTile(x, y);
						}
						break;
					case 12:
						if (x == 4) {
							tuile= new ForestTile(x, y);
						}
						if (x >= 27 && x <= 28) {
							tuile= new RiverTile(x, y);
						}
						break;
					case 13:
						if (x == 0 || x == 12 || x == 15) {
							tuile= new ForestTile(x, y);
						}
						if (x == 1) {
							tuile= new NoneTile(x, y);
						}
						break;
					case 14:
						if (x == 0 || x == 9) {
							tuile= new NoneTile(x, y);
						}
						break;
					case 15:
						if (x == 2 || x == 8 || x == 12 || x == 16 || x == 17 || x == 21) {
							tuile= new ForestTile(x, y);
						}
						if (x == 10) {
							tuile= new NoneTile(x, y);
						}
						break;
					case 16:
						if (x == 7 || x == 16 || x >= 15 && x <= 18 || x == 28) {
							tuile= new ForestTile(x, y);
						}
						if (x == 10) {
							tuile= new NoneTile(x, y);
						}
						break;
					case 17:
						if (x == 9) {
							tuile= new ForestTile(x, y);
						}
						if (x == 10) {
							tuile= new NoneTile(x, y);
						}
						break;
					case 18:
						if (x == 5 || x == 13 || x == 14 || x == 16 || x == 20) {
							tuile= new ForestTile(x, y);
						}
						if (x == 10) {
							tuile= new NoneTile(x, y);
						}
						break;
					case 19:
						if (x == 1 || x == 15 || x == 16 || x == 19 || x == 21) {
							tuile= new ForestTile(x, y);
						}
						if (x == 9) {
							tuile= new NoneTile(x, y);
						}
						break;
					default:
						
				} 

				rangee.push(tuile);
			}

			tuiles.push(rangee);
		}

		return tuiles;
	}

	calcDistance(dep, arr) {
		var coteX= Math.abs(dep.posX-arr.posX, 2);
		var coteY= Math.abs(dep.posY-arr.posY, 2);
		var distance= coteX+coteY;

		return distance;
	}
}