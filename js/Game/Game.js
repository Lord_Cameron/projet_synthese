class Game {
	constructor() {
		this.playerTeam= new PlayerTeam(); // l'équipe du joueur
		this.comTeam= new ComTeam(); // l'équipe de l'ordinateur
		this.map= this.createMap(); // la carte du jeu. constituer de tuiles
		Game.aStar= new Pathfinding(this.map); // Pathfinding
		this.phase= Unit.t_master.PLAYER; // gère les phases du jeu

		this.unitAction= null;	// l'unité sélectionné par le joueur
		this.tuileDepUnitAct= null; // la tuile de départ de l'unité sélectionné
		this.destination= null; // la destination choisi pour un déplacement
		this.unitChange= false; // Si sélectionne une unité quand une était déja sélectionnée
		this.cancelAct= false; // cancel une action 
		this.rangeIsSet= false; // détermine la porté des unité est calculé
		this.simCom= false; // si on effectu une simulation de combat pour les COM
		this.playerHasWon= false; // si les joueur gagne la partie
		this.gameIsFinished= false; // si la partie est terminé
		this.movEnCours= false; // si un mouvement est en cours
		this.inAttackRange= [];  // les unités à porté d'armes de l'unité sélectionnée
		
		this.cible= null; // la cible choisie par le joueur
		this.deadUnit= null; // une unit qui est défaites

		this.cptComGroup= 0; 
		this.cptComUnit= 0;
	}

	// crée la carte du jeu
	createMap(){
		return this.addUnitsToMap(new Map());
	}

	// ajoute les unité à la carte
	addUnitsToMap(map){
		var mapAvecUnits= map;

		this.playerTeam.units.forEach(function(unit) {
			mapAvecUnits.tuiles[unit.posY][unit.posX].unit= unit;
		}, this);

		this.comTeam.groups.forEach(function(group) {
			group.units.forEach(function(unit) {
				mapAvecUnits.tuiles[unit.posY][unit.posX].unit= unit;
			}, this);
		}, this);
		
		return mapAvecUnits;
	}

	// gère les actions du joueur (click droit et gauche)
	gestionActionJoueur (caseSelect, caseSelectAlt) {
		var caseSelect= caseSelect; // clic gauche
		var caseSelectAlt= caseSelectAlt; // clic droit
		var deadUnit= null;
		var cTemp= null; 
		this.cible= null; 
		this.deadUnit= null;
		
		// si clic droit sur case
		if (caseSelectAlt != null) {

			// si une unité est sélectionné
			if (this.unitAction != null) {
				this.cancelAction()
			}
			else {
				this.mettreEnAttente(caseSelectAlt);
			}
		}

		// si clic gauche sur case
        if (caseSelect != null) {

			// si aucune unité est sélectionnée
			if (this.unitAction == null) {

				// si une unité est sur la case sélectionnée
				if (caseSelect.unit != null){
					this.setUnitAction(caseSelect);
				}
			} 
			// si aucune destination ou cible n'est sélectionnée
			else if (this.destination == null || this.cible == null) { 
				// si une unité est sur la case sélectionnée
				if (caseSelect.unit != null) { //valide si même unit ou unit ennemis
					// si l'unité sur la case sélectionné est l'unité "activée"
					if (caseSelect.unit == this.unitAction) {
						// si l'unité s'est déplacé vers un ennemis et attend une confirmation
						if (this.unitAction.remainMov != this.unitAction.mov) {
							this.setUnitToUsed(this.unitAction);
						}

						this.unSelectAction();
					}
					// si la case sélectionnée contient une cible valide
					else if (this.cibleValide(caseSelect)) {
						this.deadUnit= new Combat(this.unitAction, caseSelect.unit, this.map).fight();
						this.cible= caseSelect.unit;

						// si une unité est morte
						if (this.deadUnit != null) {
							this.killUnit(this.deadUnit);
						}

						this.setUnitToUsed(this.unitAction);
						this.unSelectAction();
					}
					else {
						cTemp= caseSelect;
						this.cancelAction();
						this.setUnitAction(cTemp);
					}
				}
				else { 
					// si la destination choisie est valide 
					if (this.destinationValide(caseSelect)) {
						this.destination= caseSelect;
						this.movEnCours= true;
					}
				}
			}
		}
	}

	// gère les actions de l'ordinateur
	gestionActionCom() {
		var group= null;
		var unit= null;
		this.deadUnit= null;
		this.cible= null;
		
		if (this.cptComGroup < this.comTeam.groups.length) {
			group= this.comTeam.groups[this.cptComGroup];

			if (this.cptComUnit < group.units.length) {
				this.unitAction= group.units[this.cptComUnit];
				this.tuileDepUnitAct= this.map.tuiles[this.unitAction.posY][this.unitAction.posX];

				if (this.unitAction.aAgit == false) {
					this.actionCom(this.unitAction);

					if (this.cible != null) {
						this.movEnCours= true;
					}
					else {
						this.setUnitToUsed(this.unitAction);
						this.unSelectAction();
					}
				}
				this.cptComUnit++;
			}
			else {
				this.cptComUnit= 0;
			}
			this.cptComGroup++;
		}
		else {
			this.cptComGroup= 0;
		}
	}
	
	// fait attendre l'unité cliqué
	mettreEnAttente(caseArretUnit) {
		var caseUnit;

		if (caseArretUnit.unit != null) {
			caseUnit= caseArretUnit;

			if (this.unitAction == null  && !this.cancelAct) {
				if (caseUnit.unit.master == Unit.t_master.PLAYER) {
					caseUnit.unit.aAgit= true;
				}
			}
		}
	}

	// gère l'action d'une unité COM
	actionCom(unitCom) {
		var unitCom= unitCom;
		var caseUnitCom= this.map.tuiles[unitCom.posY][unitCom.posX];
		var targets= this.findTargetsAtRangeCom(unitCom);
		var possTargetName= null;
		var target= null;
		var position= {};
		var priority= {};
		var infos= {};
		
		if (targets.length > 0) {
			targets.forEach(function(possTarget) {
				possTargetName= possTarget.unit.nom;
				position[possTargetName]= this.findBestAtkPos(unitCom, possTarget);

				if (position[possTargetName] != null) {
					priority[possTargetName]= this.calcPriorityTarget(unitCom, possTarget, position[possTargetName]);
				}
			}, this);

			target= this.getTarget(priority, targets);
			
			if (target != null) {
				this.cible= target;
				this.destination= position[target.nom];
			}
		}
		else if (false) {
			// group objectives
		}
	}

	moveAndAttackTarget() {
		var unit= this.unitAction;
		var target= this.cible;
		var position= this.destination;
		var caseUnit= this.map.tuiles[unit.posY][unit.posX];
		var caseTarget= this.map.tuiles[target.posY][target.posX];
		var deadUnit= null;
		
		console.log(unit.nom + " se déplace vers " + position.posX + "/" + position.posY + " pour attaquer " + target.nom + "!!");
		this.deplacement(caseUnit, position);
		this.deadUnit= new Combat(unit, target, this.map).fight();
		this.cible= target;

		if (this.deadUnit != null) {
			this.killUnit(this.deadUnit);
		}

		this.setUnitToUsed(unit);
		this.unSelectAction();
	}

	getTarget(priority, targets) {
		var target= null;
		var targetName= null;
		var highestScore= -10000;
		
		Object.keys(priority).forEach(function(key) {
			if (priority[key] >= highestScore) {
				highestScore= priority[key];
				targetName= key;
			}
		}, this);

		targets.forEach(function(possTarget) {
			if (possTarget.unit.nom == targetName) {
				target= possTarget.unit;
			}
		}, this);

		return target;
	}

	calcPriorityTarget(unitCom, target, position) {
		var unitCom= unitCom;
		var caseUnitCom= this.map.tuiles[unitCom.posY][unitCom.posX];
		var targetMaxHp= 0;
		var targetCurHp= 0;
		var priority= 0;
		var dmgDealt= {};

		this.simCom= true;
		this.deplacement(caseUnitCom, position);
		targetMaxHp= target.unit.totalHp;
		targetCurHp= target.unit.hp;

		priority+= 100 - this.calcPriorityScore(targetCurHp, targetMaxHp);	
		dmgDealt= new Combat(unitCom, target.unit, this.map).simulation();
		
		if (targetCurHp - dmgDealt["defHP"] <= 0) {
			priority+= 100;
		}

		priority= this.calcPriorityScore(dmgDealt["defHP"], targetMaxHp);
		priority-= this.calcPriorityScore(dmgDealt["offHP"], unitCom.totalHp);
		
		this.deplacement(position, caseUnitCom);
		this.simCom= false;

		return priority;
	}

	calcPriorityScore(hp, totalHp) {
		return Math.floor(hp / totalHp * 100);
	}

	findBestAtkPos(unitCom, target) {
		var caseUnitCom= this.map.tuiles[unitCom.posY][unitCom.posX];
		var target= target;
		var possGen= [];
		var possValide= null;
		var proxim= unitCom.mov;
		var proxTmp= 0

		possGen= this.findTilesAtWeaponRange(target, caseUnitCom.unit.weapon.range);
		possGen.forEach(function(tPoss) {
			if (this.isInArray(tPoss, caseUnitCom.unit.movRange)) {
				if (tPoss.unit == null) {
					proxTmp= this.map.calcDistance(caseUnitCom, tPoss);
					
					if (proxTmp <= proxim) {
						proxim= proxTmp;
						possValide= tPoss;
					}
				}
			}
		}, this);

		return possValide;
	}

	uniteValide(unit) {
		var estValide= false; 
		
		if (unit.master == Unit.t_master.PLAYER){
			if (!unit.aAgit){
				estValide= true;
			}
		}
		
		return estValide;
	}

	setUnitAction (tuile) {
		var unit= tuile.unit;
		
		if (this.uniteValide(unit)) {
			this.unitAction= unit;
			this.tuileDepUnitAct= tuile; 
			
			this.unitChange= true;
			this.inAttackRange= this.findTargetsAtWeaponRange(tuile, unit.weapon.range, unit.master);
		}
	}

	unSelectAction() {
		this.unitAction= null;
		this.destination= null;
		this.inAttackRange= [];
	}

	cancelAction() { //a rendre plus spécifique
		if (this.unitAction != null) {
			if (!this.unitAction.aAgit) {
				if (this.destination != null) {
					this.deplacement(this.destination, this.tuileDepUnitAct);
					this.tuileDepUnitAct.unit.restoreMov(this.unitAction.mov);
				}
			}
		}

		this.unSelectAction();
		this.cancelAct= false;
	}

	restoreUnits() {
		this.rangeIsSet= false;
		this.playerTeam.units.forEach(function(unit) {
			unit.aAgit= false;
			unit.restoreMov(unit.mov);
		}, this);

		this.comTeam.groups.forEach(function(group) {
			group.units.forEach(function(unit) {
				unit.aAgit= false;
				unit.restoreMov(unit.mov);
			}, this);
		}, this);
	}

	calcRangeUnits() {
		var tuile= null;

		this.playerTeam.units.forEach(function(unit) {
			if (!unit.aAgit && this.phase != Unit.t_master.COM) {
				unit.movRange= this.calcRangeUnit(this.map.tuiles[unit.posY][unit.posX]);
			}
		}, this);

		this.comTeam.groups.forEach(function(group) {
			group.units.forEach(function(unit) {
				if (!unit.aAgit) {
					unit.movRange= this.calcRangeUnit(this.map.tuiles[unit.posY][unit.posX]);
				}
			}, this);
		}, this);
	}

	calcRangeUnit(tuile) {
		return Game.aStar.findActionRange(tuile);
	}

	cibleValide(tuile) {
		var currentTile= this.map.tuiles[this.unitAction.posY][this.unitAction.posX];
		
		var estValide= false; 
		var unit= tuile.unit;
		var distance= this.map.calcDistance(currentTile, tuile);

		if (unit.master == Unit.t_master.COM) {
			if (distance <= this.unitAction.weapon.range) {
				estValide= true;
			}
		}

		return estValide;
	}
	
	destinationValide(tuile) {
		var estValide= false; 

		if(this.unitAction.isInMovRange(tuile) && this.unitAction.remainMov == this.unitAction.mov)
			estValide= true;

		return estValide;
	}

	findTargetsAtRangeCom(unit) {
		var unit= unit;
		var targets= [];
		var directTargets= [];

		unit.movRange.forEach(function(tuile) {
			directTargets= this.findTargetsAtWeaponRange(tuile, unit.weapon.range, unit.master);

			directTargets.forEach(function(elem) {
				if (!this.isInArray(elem, targets)) {
					targets.push(elem);
				}
			}, this);

		}, this);

		return targets;
	}

	findTilesAtWeaponRange(tuile, range) {
		return this.findTargetsAtWeaponRange(tuile, range, null);
	}

	findTargetsAtWeaponRange(tuile, range, master) {
		var range= range;
		var temp= null;
		var master= master;
		var distance= 0;
		var inAttackRange= [];

		for(var y = -range; y <= range; y++) {
			for(var x = -range; x <= range; x++) {
				var tmpX=tuile.posX+x;
				var tmpY=tuile.posY+y;

				if (tmpX >= 0 && tmpX < this.map.largeur && tmpY >= 0 && tmpY < this.map.hauteur) {
					temp= this.map.tuiles[tmpY][tmpX]; 
					distance= this.map.calcDistance(tuile, temp);

					if (distance <= range && distance != 0 && distance != null) {
						if (master != null) {
							if (temp.unit != null) {
								if (temp.unit.master != master) {
									inAttackRange.push(temp);
								}
							}
						}
						else {
							inAttackRange.push(temp); //si aucun maitre. renvoi toutes les tuiles.
						}
					}
				}
			}
		}
		return inAttackRange;
	}

	deplacement(depart, arrivee) {		
		var unit= depart.unit;
		depart.unit= null;
		arrivee.unit= unit;
		unit.posX= arrivee.posX;
		unit.posY= arrivee.posY;

		if (!this.simCom) {
			this.inAttackRange= this.findTargetsAtWeaponRange(arrivee, unit.weapon.range, unit.master);

			if (this.phase == Unit.t_master.PLAYER) {
				if (this.unitAction.remainMov == this.unitAction.mov) {
					if (this.inAttackRange.length > 0) {
						this.unitAction.changeMov(this.unitAction.mov);
					}
					else {
						this.setUnitToUsed(this.unitAction);
						this.unSelectAction();
					}
				}
			}
		}

		this.movEnCours= false;
	}

	setUnitToUsed(unit) {
		unit.aAgit= true;
		this.calcRangeUnits();
	}

	killUnit(deadUnit) {
		this.map.tuiles[deadUnit.posY][deadUnit.posX].unit= null;

		if (deadUnit.master == Unit.t_master.PLAYER) {
			for (var i = 0; i < this.playerTeam.units.length; i++) {
				if (this.playerTeam.units[i] == deadUnit) {
					this.playerTeam.units.splice(i, 1);
					i--;
				}
			}
		}
		else {
			var groups= this.comTeam.groups;

			for (var i = 0; i < groups.length; i++) {
				for (var j = 0; j < groups[i].units.length; j++) {
					if (groups[i].units[j] == deadUnit) {
						groups[i].units.splice(j, 1);
						j--;
					}
				}
				
				if (groups[i].units.length <= 0) {
					groups.splice(i, 1);
					i--;
				}
			}
		}
	}

	verifGameFinished() {
		if (this.playerTeam.units.length <= 0 && this.comTeam.groups.length > 0) {
			this.playerHasWon= false;
			this.gameIsFinished= true;
		}
		else if (this.playerTeam.units.length > 0 && this.comTeam.groups.length <= 0) {
			this.playerHasWon= true;
			this.gameIsFinished= true;
		}

		return this.gameIsFinished;
	}

	estPhaseJoueur() {
		return this.phase == Unit.t_master.PLAYER;
	}

	verifPhaseTerminee() {
		var phaseTerminee= true;

		if (this.estPhaseJoueur()) {
			this.playerTeam.units.forEach(function(unit) {
				if (!unit.aAgit) {
					phaseTerminee= false;
				}
			}, this);
		}
		else {
			this.comTeam.groups.forEach(function(group) {
				group.units.forEach(function(unit) {
					if (!unit.aAgit) {
						phaseTerminee= false;
					}
				}, this);
			}, this);			
		}

		return phaseTerminee;
	}

	changementPhase() {
		var changement= null;

		if(this.verifPhaseTerminee()) {
			if (this.estPhaseJoueur()) {
				this.phase= Unit.t_master.COM;
			}
			else {
				this.phase= Unit.t_master.PLAYER;
			}

			changement= this.phase;
			this.restoreUnits();
		}

		return changement;
	}

	isInArray(elem, array) { 
		var contiens= false;
		array.forEach(function(elemTmp) {
			if (elem == elemTmp) {
				contiens= true;
			}
		}, this);

		return contiens;
	}
}