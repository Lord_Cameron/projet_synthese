class UnitAnim {
	constructor(spriteSheet, posX, posY, flip) {	
		this.state= 0;
		this.flip= flip;

		this.anim= new TiledImage(spriteSheet, 4, 1, 250, true, 1.9);
		this.anim.changeCol(0);
		this.anim.changeMinMaxInterval(0, 3);
		this.anim.flipped= flip;

		this.posX= posX*30+15;
		this.posY= posY*30+15;
		this.animate= true;
	}

	tick(aAgit) {
		this.anim.looped= this.animate;
		this.anim.tick(ctx, this.posX, this.posY);
	}

	changeSpriteSheet(spriteSheet) {
		this.anim= new TiledImage(spriteSheet, 3, 1, 250, true, 1);
		this.anim.changeCol(0);
		this.anim.changeMinMaxInterval(0, 3);
		this.anim.flipped= this.flip;
	}

	changePos(posX, posY) {
		this.posX= posX*30+15;
		this.posY= posY*30+15;
	}

	changePosAdd(x, y) {
		this.posX+= x;
		this.posY+= y;
	}
}