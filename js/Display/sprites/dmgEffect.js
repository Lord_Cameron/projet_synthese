class dmgEffect {
	constructor(posX, posY) {
		this.posX= posX;
		this.posY= posY;
		this.size = 30;
		this.opacity = 0.2;
		this.isFading= false;
	}

	tick() {
		if (this.opacity >= 1) {
			this.isFading= true;
		}

		if (!this.isFading) {
			this.opacity= this.opacity+0.20;
		}
		else {
			this.opacity= this.opacity-0.10;
		}

		if (this.opacity >= .80) {
			ctx.fillStyle = "rgba(255,0,0," + this.opacity + " )";
		}
		else {
			ctx.fillStyle = "rgba(255,255,255," + this.opacity + " )";
		}

		ctx.fillRect(this.posX*this.size-1, this.posY*this.size-1, this.size, this.size);

		return this.opacity > 0.02 //this.alive
	}
}