class phaseAnim {
	constructor(mapWidth, mapHeight, color, phase) {	
		this.mapWidth= mapWidth;
		this.mapHeight= mapHeight;
		this.color= color;
		this.phase= phase;
		this.posX= 0;
		this.posY= 200;
		this.textX= -500;
		this.textY= 330;
		this.height= 200;
		this.opacity = 0.5;
		this.isFading= false;
		this.sendMessage= false;
		this.animPhaseTerminer= false;
	}

	tick() {
		var phaseText;
		if (this.opacity >= 0.8 && !this.isFading) {
			this.sendMessage= true;
		}

		if (!this.sendMessage) {
			if (!this.isFading) {
				this.opacity= this.opacity+0.04;
			}
			else {
				this.opacity= this.opacity-0.04;
			}
		}

		if (this.opacity >= .95) {
			ctx.fillStyle = this.color + this.opacity + " )";
		}
		else {
			ctx.fillStyle = this.color + this.opacity + " )";
		}

		ctx.fillRect(this.posX, this.posY, this.mapWidth, this.height);

		if (this.sendMessage) {			
			if(this.phase == Unit.t_master.PLAYER) {
				phaseText= "PLAYER";
			}
			else {
				phaseText= "COM";
			}

			ctx.font = "90px 'Impact'";
			ctx.fillStyle = "white";
			ctx.fillText(phaseText + " PHASE", this.textX, this.textY);
			this.textX+= 30;

			if (this.textX >= this.mapWidth-20) {
				this.sendMessage= false;
				this.isFading= true;
			}
		}

		if (this.opacity <= 0.02) {
			changementPhaseEnCours= false; 
		}

		return this.opacity > 0.02;
	}
}