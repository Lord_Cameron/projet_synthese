class GameOver {
	constructor(playerHasWon) {
		this.playerHasWon= playerHasWon;
		this.height = 600;
		this.width = 900;
		this.x = 0;
		this.y = 0;
		this.textOpacity = 0;
		this.opacity = 0.2;
		this.colorIsSet= false;
		this.fadeToBlack= false;
		this.showText= true;
		this.r= 0;
		this.g= 0;
		this.b= 0;
	}

	draw(finish) {
		var finish= finish;
		var textSize= null;
		var textColor= null;
		var text= null;
		var animFinished= false;

		console.log(finish);
		

		if (this.playerHasWon) {
			text= "VICTORY"
			textColor= "rgba(0,0,255,";
			textSize= "140px";

			if (!this.colorIsSet) {
				this.colorIsSet= true;
				this.r= 255;
				this.g= 255;
				this.b= 255;
			}
		}
		else {
			text= "GAME OVER";
			textColor= "rgba(255,0,0,";
			textSize= "112px";

			if (!this.colorIsSet) {
				this.colorIsSet= true;
				this.r= 0;
				this.g= 0;
				this.b= 0;
			}
		}

		if (this.opacity >= 0.9) {
			this.sendMessage= true;
		}

		if (this.opacity < 1) {
			this.opacity= this.opacity+0.01;
		}

		if (this.sendMessage && this.textOpacity < 1) {
			this.textOpacity= this.textOpacity + 0.008;
		}

		if (finish) {
			if (!this.fadeToBlack) {
				if (this.textOpacity >= 0.98) {
					if (this.playerHasWon) {
						this.r-= 3;
						this.g-= 3;

						if (this.r <= 0 && this.g <= 0) {
							this.fadeToBlack= true;
						}
					}
					else {
						this.r+= 3;

						if (this.r >= 255) {
							this.fadeToBlack= true;
						}
					}
				}
			} 
			else {
				this.showText= false;

				if (this.r >= 0) {
					this.r-= 3;
				}

				if (this.g >= 0) {
					this.g-= 3;
				}

				if (this.b >= 0) {
					this.b-= 3;
				}
			}
		}

		ctx.fillStyle = "rgba(" + this.r + "," + this.g + "," + this.b + "," + this.opacity + " )";
		ctx.fillRect(this.x, this.y, this.width, this.height);
		
		if (this.showText) {
			ctx.font = textSize + " 'Impact'"; 
			ctx.fillStyle = textColor + this.textOpacity + " )";
			ctx.fillText(text, 200, 350);
		}

		return this.r <= 0 && this.g <= 0 && this.b <= 0 && this.fadeToBlack;
	}

}