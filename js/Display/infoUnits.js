class InfoUnit {
	constructor() {
		this.height = 100;
		this.width = 220;
		this.x = 660;
		this.y = 10;
		this.opacity = 0.8;
		this.onUnit = false;
		this.show = true;
		this.unit = null;
		this.img= new Image();
	}

	draw() {
		if (this.onUnit && this.show) {
			// ctx.fillStyle = "rgba(120,187,255," + this.opacity + " )";
			// ctx.fillRect(this.x, this.y, this.width, this.height);

			ctx.fillStyle = "rgba(0,0,0," + this.opacity + " )";
  			ctx.fillRect(this.x - (1), this.y - (1), this.width + (1 * 2), this.height + (1 * 2));

			if (this.unit.master == Unit.t_master.PLAYER){
				ctx.fillStyle = "rgba(120,187,255," + this.opacity + " )";
			}
			else{
				ctx.fillStyle = "rgba(255,157,120," + this.opacity + " )";
			}
			
			this.img.src= this.unit.getPortrait();
			ctx.fillRect(this.x, this.y, this.width, this.height);
			
			ctx.font = "18px 'Impact'";
			ctx.fillStyle = "black";
			ctx.fillText(this.unit.nom,this.x+5,this.y+18);

			ctx.fillText("HP:" + this.unit.hp,this.x+this.width/4+60,this.y+25);
			ctx.fillText("ATK:" + this.unit.atk,this.x+this.width/4+20,this.y+55);
			ctx.fillText("SPD:" + this.unit.spd,this.x+this.width/4+100,this.y+55);
			ctx.fillText("DEF:" + this.unit.def,this.x+this.width/4+20,this.y+85);
			ctx.fillText("RES:" + this.unit.res,this.x+this.width/4+100,this.y+85);

			ctx.drawImage(this.img, this.x+5, this.y+25, this.width/4, this.height-28);
		}
	}

	toggle() {
		this.show= !this.show;
	}

	setInfos(unit) {
		this.unit= unit;
	}
}