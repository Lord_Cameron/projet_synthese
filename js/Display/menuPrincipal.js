class MenuPrincipal {
	constructor() {
		this.node= document.getElementById("menuPrincipal");
		this.frame= document.body;

		this.width= 902;
		this.height= 600;
		this.node.width = this.width;
		this.node.height = this.height;
		this.node.style.width = this.width+"px";
		this.node.style.height = this.height+"px";
		
		this.node.style.margin= "auto"; 
		this.node.style.display= "block";
		this.node.style.backgroundColor= "black";
		this.node.style.textAlign= "center";
		this.node.style.backgroundImage= "URL('images/mainMenu.png')";
		this.node.style.backgroundSize= "100% 100%";
		this.node.style.backgroundPosition= "center";
	}

	afficher() {
		this.node.style.display= "block";
	}

	cacher() {
		this.node.style.display= "none";
	}

}