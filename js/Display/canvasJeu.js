class CanvasJeu {
	constructor(map) {
		this.node= document.getElementById("canvasJeu");
		this.container= document.getElementById("main");

		this.width= 900;
		this.height= 600;
		this.node.width = this.width;
		this.node.height = this.height;
		
		this.node.style.margin= "auto";
		this.node.style.display= "none";

		this.tailleCases= 30;
		this.nbCasesLignes= this.height / this.tailleCases;
		this.nbCasesCols = this.width / this.tailleCases;

		this.map= map;
		this.spriteList= [];

		this.tuileColorTemp= null;
		this.unitAffTemp= null;
		this.caseSelect= null;
		this.caseSelectAlt= null;
		this.animMovEnCours= false;
		this.animMovTerminer= false;
		// this.animPhaseEnCours= false;
		this.endGameOver= false;

		this.rightClick= false;
		this.showGameOverScreen= false;

		this.mousePosX= 0;
		this.mousePosY= 0;
		this.mouseCaseX= 0;
		this.mouseCaseY= 0;

		this.aStar= null;
		this.path= [];
		this.pathCpt= 0;
		this.currentPos= {};
		this.vitesseMov= 5;

		this.gameOverScreen= null;
		this.infoUnit= new InfoUnit();
		this.infoCombat= new InfoCombat(this.map);

		var canvas= this;
		this.node.onmousemove = function(e) {
			canvas.mousePosX = e.pageX - this.offsetLeft;
			canvas.mousePosY = e.pageY - this.offsetTop;
			
			canvas.mouseCaseX= Math.floor(canvas.mousePosX/canvas.tailleCases);
			canvas.mouseCaseY= Math.floor(canvas.mousePosY/canvas.tailleCases);

			if (canvas.mouseCaseX < 0) {
				canvas.mouseCaseX= 0;
			}
			else if (canvas.mouseCaseX > canvas.nbCasesCols-1) {
				canvas.mouseCaseX= canvas.nbCasesCols-1;
			}

			if (canvas.mouseCaseY < 0) {
				canvas.mouseCaseY= 0;
			}
			else if (canvas.mouseCaseY > canvas.nbCasesLignes-1) {
				canvas.mouseCaseY= canvas.nbCasesLignes-1;
			}

			if (canvas.map.tuiles[canvas.mouseCaseY][canvas.mouseCaseX].unit != null){
				var unitHover= canvas.map.tuiles[canvas.mouseCaseY][canvas.mouseCaseX].unit;
				canvas.infoUnit.setInfos(unitHover);
				canvas.infoUnit.onUnit= true;

				if (unitHover.master != Unit.t_master.PLAYER) {
					canvas.infoCombat.setDefUnit(unitHover);
					canvas.infoCombat.showInfo();
				}
				else {
					canvas.infoCombat.hideInfo();
				}
			
				canvas.unitAffTemp= canvas.map.tuiles[canvas.mouseCaseY][canvas.mouseCaseX].unit;
			}
			else {
				canvas.infoCombat.hideInfo();
				// canvas.infoUnit.onUnit= false;
				// canvas.unitAffTemp= null;
			}
		}

		this.node.onclick = function(e) {
			canvas.caseSelect= canvas.map.tuiles[canvas.mouseCaseY][canvas.mouseCaseX];
			canvas.endGameOver= true;
		}

		this.node.onwheel = function(e) {
			console.log(canvas.map.tuiles[canvas.mouseCaseY][canvas.mouseCaseX]);
			console.log(canvas.mouseCaseX + ":" + canvas.mouseCaseY);

			canvas.infoUnit.toggle();
		}
		
		this.node.oncontextmenu = function(e) {
			canvas.caseSelectAlt= canvas.map.tuiles[canvas.mouseCaseY][canvas.mouseCaseX];
			canvas.rightClick= true;
			return false;
		}
	}

	afficher(map) {
		this.container.style.backgroundImage= "URL('images/map/background_2.png')";
		this.container.style.backgroundSize= "cover";
		this.node.style.display= "block";
		ctx.clearRect(0,0,1000,1000);
		this.drawMap();
		this.drawEffects();

		if (this.infoUnit != null) {
			this.infoUnit.draw();
		}

		if (this.infoCombat.show) {
			this.infoCombat.draw(this.mousePosX, this.mousePosY);
		}
	}

	cacher() {
		this.node.style.display= "none";
	}

	dessinerGrille() {
		for (var x = 0; x <= this.nbCasesCols; x++) {
			ctx.moveTo(x * this.tailleCases, 0);
			ctx.lineTo(x * this.tailleCases, this.height);
		}

		for (var x = 0; x <= this.nbCasesLignes; x++) {
			ctx.moveTo(0, x * this.tailleCases);
			ctx.lineTo(this.width, x * this.tailleCases);
		}

		ctx.strokeStyle = "white";
		ctx.stroke();
	}

	drawMap() {
		var img= null;

		for (var y = 0; y < this.nbCasesLignes; y++) {
			for (var x = 0; x < this.nbCasesCols; x++) {

				if (this.map.tuiles[y][x].color != null) {
					ctx.fillStyle = this.map.tuiles[y][x].color;
					ctx.fillRect(x*this.tailleCases, y*this.tailleCases, this.tailleCases, this.tailleCases);
				}
				
				if (this.map.tuiles[y][x].unit != null) {
					var unit= this.map.tuiles[y][x].unit;

					if (!this.animMovEnCours) {
						unit.anim.changePos(unit.posX, unit.posY);
					}

					unit.anim.animate= !unit.aAgit;
					unit.anim.tick(unit.aAgit);
				}
			}
		}

		if (this.rightClick) {
			this.rightClick= false;
		}
	}

	drawEffects() {
		
		for (var i= 0; i < this.spriteList.length; i++) {
			var alive= this.spriteList[i].tick();

			if (!alive) {
				this.spriteList.splice(i, 1)
				i--;
			}
		}
	}

	afficherAnimPhase(nouvellePhase) {
		var nouvellePhase= nouvellePhase;
		var phase= null;
		var color;
		changementPhaseEnCours= true;

		if(nouvellePhase == Unit.t_master.PLAYER) {
			color= Tile.t_colors.BLUE;
			phase= Unit.t_master.PLAYER;
		}
		else {
			color= Tile.t_colors.RED;
			phase= Unit.t_master.COM;
		}

		this.spriteList.push(new phaseAnim(this.width, this.height, color, phase));
	}

	ajoutAnimEffetAtk(cible, deadUnit) {
		var cible= cible;
		var deadUnit= deadUnit;
		
		if (deadUnit == null)  {
			if (cible != null) {
				this.spriteList.push(new dmgEffect(cible.posX, cible.posY));
			}
		}
		else {
			this.spriteList.push(new dmgEffect(cible.posX, cible.posY)); //TEMP
		}
	}

	animActionCom(infos) {
		var aStar= new Pathfinding(this.map);
		var path= null;
		var unit= infos["unit"];
		var unitTile= this.map.tuiles[unit.posY][unit.posX];
		var target= infos["target"];
		var position= infos["position"];

		path= aStar.getPath(unitTile, position);
		console.log(path);
		console.log(target);
		console.log(unitTile);
		console.log(position);

		return false;
	}

	animMouvement(depart, arrive) {
		var depart= depart;
		var arrive= arrive;
		var unit= this.map.tuiles[depart.posY][depart.posX].unit;
		
		if (this.aStar == null) {
			this.animMovEnCours= true;
			this.aStar= new Pathfinding(this.map);
			this.path= this.aStar.getPath(depart, arrive);
			this.pathCpt= 0;
		}
		
		this.goToTile(unit.anim, unit);
	}

	goToTile(unitAnim, unit) {
		var tile= null;
		var addX= 0;
		var addY= 0;
		var posX= 0;
		var posY= 0;

		if (this.pathCpt < this.path.length) {
			tile= this.path[this.pathCpt];

			posX= tile.posX*30+15;
			posY= tile.posY*30+15;

			this.currentPos["x"]= unitAnim.posX;
			this.currentPos["y"]= unitAnim.posY;

			if (this.currentPos["x"] < posX) {
				addX= this.vitesseMov;
			}
			else if (this.currentPos["x"] > posX) {
				addX= -this.vitesseMov;
			}
			else if (this.currentPos["y"] < posY) {
				addY= this.vitesseMov;
			}
			else if (this.currentPos["y"] > posY) {
				addY= -this.vitesseMov;
			}
			else {
				if (this.pathCpt >= 0) {
					this.pathCpt++;
				}
			}
			
			unitAnim.changePosAdd(addX, addY);
		}
		else {
			this.aStar= null;
			this.animMovTerminer= true;
		}
	}

	afficherCasesMouvement(unit, phase) {
		var unit= unit;
		var tmpX= 0;
		var tmpY= 0;
		var distance= 0;
		var tuile= null;
		this.tuileColorTemp= [];

		if (!unit.aAgit && !this.animMovEnCours && phase != Unit.t_master.COM) {
			this.colorTilesAtWeaponRange(this.map.tuiles[unit.posY][unit.posX], unit.weapon.range);

			if (unit.master == Unit.t_master.PLAYER) {
				if (unit.remainMov == unit.mov) {
					unit.movRange.forEach(function(tuile) {
						this.colorTilesAtWeaponRange(tuile, unit.weapon.range);
					}, this);

					unit.movRange.forEach(function(tuile) {
						tuile.setColorTo(Tile.t_colors.BLUE) 
					}, this);
				}
				this.map.tuiles[unit.posY][unit.posX].setColorTo(Tile.t_colors.BLUE);
			}
			else {
				unit.movRange.forEach(function(tuile) {
					this.colorTilesAtWeaponRange(tuile, unit.weapon.range);
				}, this);
			}
		}
	}

	colorTilesAtWeaponRange(tuile, range) {
		var tuile= tuile;
		var range= range;
		var temp= null;
		var tmpX= 0;
		var tmpY= 0;
		var distance= 0;

		for (var x= -range; x <= range; x++) {
			for (var y= -range; y <= range; y++) {
				tmpX=tuile.posX+x;
				tmpY=tuile.posY+y;

				if (tmpX >= 0 && tmpX < this.nbCasesCols && tmpY >= 0 && tmpY < this.nbCasesLignes) {
					temp= this.map.tuiles[tmpY][tmpX];
					distance= this.map.calcDistance(tuile, temp);
					
					if (distance <= range) {
						if (temp.color == null) {
							temp.setColorTo(Tile.t_colors.RED) 
							this.tuileColorTemp.push(temp);
						}
					}
				}
			}
		}
	}

	effacerCasesMouvements() {
		if (this.tuileColorTemp != null){
			for (var t in this.tuileColorTemp) {
				this.tuileColorTemp[t].setColorTo(null);
			}
			
			this.tuileColorTemp= [];
		}
	}

	drawGameOverScreen(playerHasWon) {
		var canvas= this;
		var gameOverFinished= false;

		if (this.gameOverScreen == null) {
            this.gameOverScreen= new GameOver(playerHasWon);
			this.showGameOverScreen= true;
			this.endGameOver= false;
        }

		if (this.showGameOverScreen) {
        	gameOverFinished= this.gameOverScreen.draw(this.endGameOver);
		}

		return gameOverFinished;
	}
}