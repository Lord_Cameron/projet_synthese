class InfoCombat {
	constructor(map) {
		this.map= map;
		this.show = false;
		this.offUnit = null;
		this.defUnit = null;
		this.offImg= new Image();
		this.defImg= new Image();
	}

	draw(posX, posY) {
		
		if (this.show && this.offUnit != null && this.defUnit != null) {
		 	var InfoCombat= new Combat(this.offUnit, this.defUnit, this.map).simulation();
			
			ctx.font = "18px 'arial'";
			ctx.fillStyle = "white";
			ctx.fillText("-" + InfoCombat["offHP"], this.offUnit.posX*30+2, this.offUnit.posY*30+20);
			ctx.fillText("-" + InfoCombat["defHP"], this.defUnit.posX*30+2, this.defUnit.posY*30+20);	
		}
	}

	showInfo() {
		this.show= true;
	}

	hideInfo() {
		this.show= false;
		this.defUnit= null;
	}

	setOffUnit(offUnit) {
		this.offUnit= offUnit;
	}

	setDefUnit(defUnit) {
		var dist= 100;

		if (this.offUnit != null) {
			dist= this.map.calcDistance(this.offUnit, defUnit);

			if (dist <= this.offUnit.weapon.range && dist > 0) {
				this.defUnit= defUnit;
			}
		}
	}
}