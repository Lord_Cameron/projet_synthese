var gameState= 0; // détermine l'état du jeu (1: jeu en marche // 2: fin du jeu)
var ctx= null;  // context 2d
var canvas= null;   // canvas d'affichage du jeu
var menuPrincipal= null;    // menu principal pour lancer le jeu.
var game= null; // gère la partie -> modèle
var clock= 0;   // rythme le tour des COM
var pause= false; // met le tick en pause lors d'un retour sur le menu.
var changementPhaseEnCours= false; // sert pour la synchro de l'anim du chagement de phase.

var audio= null;    //  gère l'audio du jeu
var s_mainTheme= 'audio/music/FireEmblemTheme.mp3';
var s_playerPhase= 'audio/music/WindsAcrossthePlains.mp3';
var s_comPhase= 'audio/music/EnemiesAppear.mp3';
var s_gameOver= 'audio/music/GameOver.mp3';
var s_victory= 'audio/music/Triumph.mp3';

window.onload = function () {
	classInit();
    setAudio(s_mainTheme);

    ctx= document.getElementById("canvasJeu").getContext("2d");
    menuPrincipal= new MenuPrincipal();

    document.getElementById("playButton").onclick= function() {
        changeAudio(s_playerPhase);
        game= new Game();
        canvas= new CanvasJeu(game.map);
        gameState= 1;
        menuPrincipal.cacher();
        canvas.afficher(game.map); 
        pause= false;
        tick();
    }
}

//  sert de gameLoop
function tick() {
    var endGame= null; 

    if (gameState == 1) {
        if (clock == 10000) {
            clock= 0;
        }

        run(); 
        draw();
    }
    else if (gameState == 2) { 
        endGame= canvas.drawGameOverScreen(game.playerHasWon);

        if (endGame) {
            changeAudio(s_mainTheme);
            endGame= false;
            pause= true;
            canvas.cacher();
            menuPrincipal.afficher();
        }
    }
    
    if (!pause) {
        window.requestAnimationFrame(tick);
    }
}

// Gère et synchronise le jeu et l'affichage
function run() {
    var nouvellePhase; 

    // si la portée des unité à été calculer
    if (!game.rangeIsSet){ 
        game.calcRangeUnits();
        game.rangeIsSet= true;
    }

    // si un mouvement est en cours et l'anim du mouvemnent n'est pas terminer
    if (game.movEnCours && !canvas.animMovTerminer) { 
        canvas.animMouvement(game.tuileDepUnitAct, game.destination);

        // si une unité est sélectionné et la phse est celle du COM
        if (game.unitAction != null && game.phase == Unit.t_master.COM) {
            canvas.infoUnit.setInfos(game.unitAction);
        }
    }
    else {
        // si un mouvement est en cours
        if(game.movEnCours) {

            // si c'est la phase du joueur
            if (game.phase == Unit.t_master.PLAYER) {
	            game.deplacement(game.tuileDepUnitAct, game.destination);
            }
            else {
                game.moveAndAttackTarget();
                canvas.ajoutAnimEffetAtk(game.cible, game.deadUnit);
            }

            game.movEnCours= false;
            canvas.animMovTerminer= false;
			canvas.animMovEnCours= false;
        }

        nouvellePhase= game.changementPhase();

        // si on change de phase
        if (nouvellePhase != null) {
            canvas.afficherAnimPhase(nouvellePhase);
            canvas.unitAffTemp= null;

            // si la nouvelle phase est celle du joueur
            if (nouvellePhase == Unit.t_master.PLAYER) {      
                changeAudio(s_playerPhase);
            }
            else {
                changeAudio(s_comPhase);
            }
        }    

        // si un changement de phase des pas en cours
        if (!changementPhaseEnCours) {

            // si la phase est celle du joueur
            if (game.estPhaseJoueur()) {
                game.gestionActionJoueur(canvas.caseSelect, canvas.caseSelectAlt); 
                canvas.infoCombat.setOffUnit(game.unitAction);
                canvas.ajoutAnimEffetAtk(game.cible, game.deadUnit);
            }
            else {
                if (clock % 5 == 0) {
                    game.gestionActionCom(); 
                }
            }
        }
    }

    clock++;
    canvas.caseSelect= null;
    canvas.caseSelectAlt= null;
}

// Gère l'affichage du jeux
function draw() {
    // si une unité est sélectionné et on n'a pas changer d'unité
    if (game.unitAction != null && !game.unitChange) {
        canvas.effacerCasesMouvements();
        canvas.afficherCasesMouvement(game.unitAction, game.phase);
    } 
    else if (canvas.unitAffTemp != null && game.unitAction == null) {
        canvas.effacerCasesMouvements();
        canvas.afficherCasesMouvement(canvas.unitAffTemp, game.phase);
    }
    else {
        canvas.effacerCasesMouvements();
        game.unitChange= false;
    }
    
    // si la partie est terminé
    if(game.verifGameFinished()) {
        gameState= 2;

        if (game.playerHasWon) {
            changeAudio(s_victory);
        }
        else {
            changeAudio(s_gameOver);
        }
    }

    canvas.afficher(game.map);
}

function setAudio(tune) {
    audio = new Audio(tune);
    audio.addEventListener('ended', function() {
        this.currentTime = 0;
        this.play();
    }, false);
    
    audio.play();

    return audio;
}

function changeAudio(tune) {
    audio.src = tune;
    audio.load();
    audio.play();
}

// initialise les classes des unité pour avoir accès au 
// struct (pour constantes) pour l'initialisation du jeu.
function classInit() {
    new Knight("init", 3, 16, 1, ""); 
    new Mercenary("init", 4, 16, 1, "");
    new Cleric("init", 5, 16, 1, "");
    new Shaman("init",  3, 17, 1, "");
    new Mage("init",  4, 17, 1, "");
    new Warrior("init", 5, 17, 1, "");
}